﻿using UnityEngine;
using System.Collections;

public class Lighting  : MonoBehaviour {
	private bool m_bLightOn;

	// Use this for initializatio
	void Start () 
	{

	}
	
	// Update is called once per frame
	
	void Update()
	{  
		if (m_bLightOn)
		{
			RenderSettings.ambientLight = Color.Lerp(RenderSettings.ambientLight, Color.white, 0.0045f);
			
			if ((new Vector3(RenderSettings.ambientLight.r, RenderSettings.ambientLight.g, RenderSettings.ambientLight.b)  - new Vector3(1,1,1)).magnitude < 0.1f)
			{
				RenderSettings.ambientLight = Color.black;
				m_bLightOn = false;
			}
		}
		else
		{	
			if (Random.Range(0,14) == 0)
			{
				m_bLightOn = true;
			}
		}
	}
}
