﻿using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour
{
	public bool facingRight = true;			// For determining which way the player is currently facing.

	public float maxSpeed = 5f;			// The fastest the player can travel in the x axis.
	
	Animator anim;					// Reference to the player's animator component.

	public bool grounded = false;
	public bool sideGroundedLeft = false;
	public bool sideGroundedRight = false;
	public Transform groundCheck;
	public Transform sideGroundCheckLeft;
	public Transform sideGroundCheckRight;
	float groundRadius = 0.2f;
	public LayerMask whatIsGround;
	public LayerMask whatIsSideGroundLeft;
	public LayerMask whatIsSideGroundRight;

	public float jumpForce = 200f;
	public float lurchForce = 150f;
	public float headSpin = 1000f;
	public Vector2 torqueAmount;


	void Start()
	{
		// Setting up references.
		anim = GetComponent<Animator>();
	}

	void FixedUpdate ()
	{
		grounded = Physics2D.OverlapCircle (groundCheck.position, groundRadius, whatIsGround);
		anim.SetBool ("Ground", grounded);

		sideGroundedLeft = Physics2D.OverlapCircle (sideGroundCheckLeft.position, groundRadius, whatIsSideGroundLeft);
		anim.SetBool ("SideGroundLeft", sideGroundedLeft);

		sideGroundedRight = Physics2D.OverlapCircle (sideGroundCheckRight.position, groundRadius, whatIsSideGroundRight);
		anim.SetBool ("SideGroundRight", sideGroundedRight);

		anim.SetFloat ("vSpeed", rigidbody2D.velocity.y);

		// Cache the horizontal input.
		float move = Input.GetAxis("Horizontal");

		anim.SetFloat ("Speed", Mathf.Abs (move));

		//rigidbody2D.AddTorque(-move * torqueAmount.x);
		rigidbody2D.velocity = new Vector2 (move * maxSpeed, rigidbody2D.velocity.y);

		// If the input is moving the player right and the player is facing left...
		if(move > 0 && !facingRight)
			// ... flip the player.
			Flip();
		// Otherwise if the input is moving the player left and the player is facing right...
		else if(move < 0 && facingRight)
			// ... flip the player.
			Flip();
	}

	void Update()
	{
		if (grounded && Input.GetKeyDown (KeyCode.Space)) 
		{
			anim.SetBool ("Ground", false);
			rigidbody2D.AddForce (new Vector2 (0, jumpForce));
		}

		if (sideGroundedLeft && Input.GetKeyDown (KeyCode.LeftAlt)) 
		{
			if(facingRight)
			{
				anim.SetBool ("SideGroundLeft", false);
				//rigidbody2D.AddForce (new Vector2 (lurchForce, 500));
				rigidbody2D.AddTorque(-headSpin);
				Debug.Log("Facing Right - Left Side Spin");
			}
			else if(!facingRight)
			{
				anim.SetBool ("SideGroundLeft", false);
				//rigidbody2D.AddForce (new Vector2 (lurchForce, 500));
				rigidbody2D.AddTorque(headSpin);
				Debug.Log("Facing Left - Left Side Spin");
			}
		}

		if (sideGroundedRight && Input.GetKeyDown (KeyCode.LeftAlt)) 
		{
			if(facingRight)
			{
				anim.SetBool ("SideGroundRight", false);
				//rigidbody2D.AddForce (new Vector2 (lurchForce, 500));
				rigidbody2D.AddTorque(headSpin);
				Debug.Log("Facing Right - Right Side Spin");
			}
			if(facingRight)
			{
				anim.SetBool ("SideGroundRight", false);
				//rigidbody2D.AddForce (new Vector2 (lurchForce, 500));
				rigidbody2D.AddTorque(headSpin);
				Debug.Log("Facing Left - Right Side Spin");
			}
		}
    }
	
	void Flip ()
	{
		// Switch the way the player is labelled as facing.
		facingRight = !facingRight;

		// Multiply the player's x local scale by -1.
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

	 // drag the blood prefab here

	void OnCollisionEnter2D (Collision2D col) 
	{
		if(col.gameObject.tag == "Finish")
		{
			Application.LoadLevel(Application.loadedLevel + 1);
		}

		if(col.gameObject.tag == "End")
		{
			Application.LoadLevel(0);
		}
//		if(col.gameObject.tag == "Obstacle")
//		{
//			var bloodPrefab = gameObject;
//			// get the first contact point info
//			var contact = col.contacts[0]; 
//			// find the necessary rotation...
//			var rot = Quaternion.FromToRotation(Vector2.up, contact.normal);
//			Instantiate(bloodPrefab, contact.point, rot); // and make the enemy bleed
//		}
	}
}

