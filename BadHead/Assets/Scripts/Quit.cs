﻿using UnityEngine;
using System.Collections;

public class Quit : MonoBehaviour {

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		EscapePresses();
	
	}
	void EscapePresses()
	{
		//allows user to qui any time 
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			Application.Quit();
		}
	}
	void QuitGame()
	{
		Application.Quit();
	}
}
