﻿using UnityEngine;
using System.Collections;

public class PauseMenu : MonoBehaviour {

	public bool isPaused = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		//var isPaused = false;
		if( Input.GetKeyDown(KeyCode.Escape))
		{
			if (!isPaused)
			{
				Time.timeScale = 0;
				isPaused = true;
			}
			else 
			{
				Time.timeScale = 1;
				isPaused = false;
			}
		}

	}

	void OnGUI () {
		if(isPaused)
		{
		// Make a background box
			GUI.Box(new Rect(Screen.width/2 - 150,Screen.height/2 - 200,300,300), "Pause Menu");
		
		// Make the first button. If it is pressed, Application.Loadlevel (1) will be executed
			if(GUI.Button(new Rect(Screen.width/2 - 75,Screen.height/2 - 140,160,40), "Restart Level")) {
				Application.LoadLevel(Application.loadedLevel);
				Time.timeScale = 1;
				isPaused = false;
			}
		
		// Make the second button.
			if(GUI.Button(new Rect(Screen.width/2 - 75,Screen.height/2 - 60,160,40), "Main Menu")) {
				Application.LoadLevel(0);
				Time.timeScale = 1;
				isPaused = false;
			}

			if(GUI.Button(new Rect(Screen.width/2 - 75,Screen.height/2 + 20,160,40), "Exit Game")) {
				Application.Quit();
			}
		}
	}
}
