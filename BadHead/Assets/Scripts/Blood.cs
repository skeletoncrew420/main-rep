﻿using UnityEngine;
using System.Collections;

public class Blood : MonoBehaviour {
public Transform head;
public Transform blood;
public bool bloodSpawn = false;

	// Use this for initialization
	void Start () {
		Instantiate (blood, head.transform.position, head.transform.rotation);	
	}
	
	// Update is called once per frame
	void Update () {

	}
}
